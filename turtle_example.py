import turtle
import time

screen = turtle.Screen()
screen.addshape("turtle.gif")
screen.bgcolor("light blue")

ella = turtle.Turtle()


ella.shape("turtle.gif")
ella.color("magenta")

def draw_star():
    ella.hideturtle()
    ella.penup()
    ella.goto(-200, -200)
    ella.pendown()
    ella.showturtle()
    time.sleep(2)
    for i in range(5):
        ella.forward(200)
        ella.right(144)
        ella.write(str(i+1),font=("Arial", 25, "bold"), align="right")
        time.sleep(3)

def draw_shape(sides):
    ella.hideturtle()
    ella.penup()
    ella.goto(-200, -300)
    ella.pendown()
    ella.showturtle()
    time.sleep(2)
    for i in range(0,sides):
        ella.fd(250)
        ella.lt(360/sides)
        ella.write(str(i+1),font=("Arial", 25, "bold"), align="right")
        time.sleep(2)

if __name__ == '__main__':
    SIDES = 5


    ella.begin_fill()
    #draw_star()
    draw_shape(SIDES)
    ella.end_fill()
    ella.hideturtle()
    #ella.write("Yay Ella!",font=("Arial", 25, "bold"), align="right")
    screen.exitonclick()
